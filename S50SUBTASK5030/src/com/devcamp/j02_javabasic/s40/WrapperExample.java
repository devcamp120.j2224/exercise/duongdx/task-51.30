package com.devcamp.j02_javabasic.s40;

public class WrapperExample {
    public static void autoBoxing() {
        byte bte = 11 ;
        short sh = 22 ;
        int it = 33 ;
        long lng = 44 ;
        float fat = 55.0F ;
        double dbl = 66.0D ;
        char ch = 'z';
        boolean bool = true ;
        /*
         * autoboxing là chế độ chuyển đổi từ chế độ nguyên thủy sang objec 
         * của wrapper Class tương ứng 
         */
        Byte byteobj = bte ;
        Short shortobj = sh ;
        Integer intobj = it ;
        Long longobj = lng ;
        Float floatobj = fat ;
        Double doubleobj = dbl ;
        Character charobj = ch ;
        Boolean boolobj = bool ;

        System.out.println("--Printing object values( in gia tri cua object)---");
        System.out.println("byte object:  " + byteobj );
        System.out.println("Short object:  " + shortobj );
        System.out.println("Integer object:  " + intobj );
        System.out.println("Long object:  " + longobj );
        System.out.println("Float object:  " + floatobj );
        System.out.println("Double object:  " + doubleobj );
        System.out.println("Character object:  " + charobj );
        System.out.println("Boolean object:  " + boolobj );
    }

    public static void unBoxing() {
        byte bte = 101 ;
        short sh = 202 ;
        int it = 303 ;
        long lng = 404 ;
        float fat = 505.0F ;
        double dbl = 606.0D ;
        char ch = 'b';
        boolean bool = false ;
        // autoboxing : Converting primitives into object
        Byte byteobj = bte ;
        Short shortobj = sh ;
        Integer intobj = it ;
        Long longobj = lng ;
        Float floatobj = fat ;
        Double doubleobj = dbl ;
        Character charobj = ch ;
        Boolean boolobj = bool ;
        /*
         * Unboxing là cơ chế tự động chuyển đổi các object
         * của wrapper class sang kiểu dự liệu nguyên thủy
         */
        byte bytevalue = byteobj ;
        short shortvalue = shortobj ;
        int intvalue = intobj ;
        long longvalue = longobj ;
        float floatvalue = floatobj ;
        double doublevalue = doubleobj ;
        char charvalue = charobj ;
        boolean boolvalue = boolobj ;

        System.out.println("--Printing primitive values( in gia tri kieu du lieu nguyen thuy)---");
        System.out.println("byte value:  " + bytevalue );
        System.out.println("Short value:  " + shortvalue );
        System.out.println("Integer value:  " + intvalue );
        System.out.println("Long value:  " + longvalue );
        System.out.println("Float value:  " + floatvalue );
        System.out.println("Double value:  " + doublevalue );
        System.out.println("Character value:  " + charvalue );
        System.out.println("Boolean value:  " + boolvalue );
    }
    public static void main(String[] args) {
        WrapperExample.autoBoxing();
        WrapperExample.unBoxing();
    }
}
